#!/usr/bin/python3
import argparse
import sys
import subprocess
import os
from os.path import join

parent_dir = sys.path[0]
printers_dir = os.path.abspath(join(parent_dir, 'printers'))
test_dir = join(parent_dir, 'tests')
cmake_src_path = join(test_dir, 'cpp_src_test')
cmake_build_path = join(cmake_src_path, 'cmake_build')
cmake_binary = join(cmake_build_path, 'test_bin')

py_test_file = join(test_dir, 'test_qtprinters.py')


def cmake_build(prefix_path, source_path="..", build_path='./build', cmake_path='cmake'):
    assert os.path.isdir(source_path)
    if cmake_path != 'cmake':
        assert os.path.isfile(cmake_path)

    cmake_args = [cmake_path, "-DCMAKE_BUILD_TYPE=Debug", "-S", source_path, '-B', build_path]
    if os.name == 'nt':
        cmake_args += ["-G", "CodeBlocks - MinGW Makefiles"]

    if prefix_path is not None or prefix_path != "":
        cmake_args += ["-DCMAKE_PREFIX_PATH=%s" % prefix_path]

    cmake_args += ["-S", source_path, '-B', build_path]
    cmake_build_args = [cmake_path, "--build", build_path]

    print("[INFO][CMAKE]: Building CMakeLists file")
    subprocess.check_call(cmake_args)
    subprocess.check_call(cmake_build_args)
    # TODO: check if the cmake builds correctly and throw errors if not


def run_debugger(gdb_path, py_file):
    if gdb_path == "":
        gdb_path = "gdb"

    print("[INFO][RUN_DEBUGGER]: Adding Printers Directory to the PYTHONPATH environment variable")
    environ = os.environ.copy()
    environ['PYTHONPATH'] = "".join([environ.get('PYTHONPATH', ''), os.pathsep, parent_dir, os.pathsep, test_dir])
    print("[INFO][PYTHON_ENV_PATH]", environ['PYTHONPATH'])
    # TODO disable system internal printers
    # TODO enable the printers to debug
    dbg_args = [gdb_path, '--nx']
    dbg_args += ['-q', '--batch', '-x', py_file]
    dbg_args.append(cmake_binary)
    print('[INFO][RUN_DEBUGGER]: --------------------- Starting GDB ------------------------')
    return subprocess.call(dbg_args, env=environ) == 0


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--prefix_path', '-pp', default="", help='to add arguments like cmake prefix path')
    return parser.parse_args()


cmake_args = parse_args()
# os.makedirs(cmake_build_path, True)
cmake_build(cmake_args.prefix_path, cmake_src_path, cmake_build_path)
run_debugger('gdb', py_test_file)
