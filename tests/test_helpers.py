import gdb
from unittest import TestCase


class PrettyPrinterTest(TestCase):
    """Base class for all printer tests"""

    def get_printer_result(self, cpp_variable_name):
        """Get pretty-printer output for cpp variable with a specified name

        :param cpp_variable_name: Name of a C variable
        :return: (string, [children], display_hint)
        """

        value = gdb.parse_and_eval(cpp_variable_name)
        pretty_printer = gdb.default_visualizer(value)
        self.assertIsNotNone(pretty_printer, 'Pretty printer was not registered')

        if hasattr(pretty_printer, 'children'):
            children = list(pretty_printer.children())
            for name, _ in children:
                self.assertIsInstance(name, str)
        else:
            children = None

        if hasattr(pretty_printer, 'display_hint'):
            if pretty_printer.display_hint() is not None:
                self.assertIsInstance(pretty_printer.display_hint(), str)
            display_hint = pretty_printer.display_hint()
        else:
            display_hint = None

        to_string = pretty_printer.to_string()

        return (to_string, display_hint, children)

    def test_initialized_with_no_variables(self):
        pass

    def test_assigned_variables(self):
        pass

    def check_equals(self, variable, string, display_hint, children):
        """Checks if the pretty-printer's variables are equal to the expected output

        Args:
            self (PrettyPrinterTest): The test class
            variable (str): the called cpp function
            string (Any): the expected to_string value
            display_hint (Any): the expected display_hint value
            children (Any): the expected children value 
        """
        org_string, org_display_hint, org_children = self.get_printer_result(variable)
        if isinstance(org_string, gdb.Value):
            org_string = to_python_value(org_string)
        self.assertEqual(org_string, string)
        self.assertEqual(org_display_hint, display_hint)
        self.assertEqual(tuples_to_list(org_children), children)

    def __str__(self):
        return '{}::{}'.format(self.__class__.__name__, self._testMethodName)


def _stop_after_a_function(start_function, break_function='init_breakpoint', silent=True):
    """
       Run until the end of a specified C++ function in another C++ function

    Args:
        start_function (str):  C++ start function name
        break_function: C++ break function name (str)
        silent (bool):  if the output is passed to stdout

    Returns:
    """

    bp = gdb.Breakpoint(function=start_function, internal=True, temporary=True)
    bp.silent = silent
    gdb.execute('run', to_string=silent)

    bp = gdb.Breakpoint(function=break_function, internal=True, temporary=True)
    bp.silent = silent
    gdb.execute('continue', to_string=silent)

    bp = gdb.FinishBreakpoint(internal=True)
    bp.silent = silent
    gdb.execute('continue')


def start_at_function(function_name, silent=True):
    """
    the breakpoint is placed at the start of the function
    :param function_name: the name of function to place the break point
    :param silent: if the output is passed to stdout
    """
    bp = gdb.Breakpoint(function=function_name, internal=True, temporary=True)
    bp.silent = silent
    gdb.execute('run', to_string=silent)


def stop_after_end_function(function_name, silent=True):
    """
    This breaks at a function named 'init_breakpoint' in the @p function_name

    Args:
        function_name (str): the name of the function that has an init breakpoint
        silent (bool): if True the output is passed to stdout

    Returns:

    """
    _stop_after_a_function(function_name, 'end_breakpoint', silent)


def stop_after_init_function(function_name, silent=True):
    """
    This breaks at a function named 'init_breakpoint' in the @p function_name

    Args:
        function_name (str): the name of the function that has an init breakpoint
        silent (bool): if the output is passed to stdout

    Returns:

    """
    _stop_after_a_function(function_name, 'init_breakpoint', silent)


def to_python_value(gdb_value):
    """Convert a gdb.Value to its python equivalent"""

    # Some printers requires other printers to show their to_string value
    #
    # for example QDir may return a gdb.Value of type QString in its to_string method,
    # so we need to convert that to a python value, gdb automatically does this when debugging
    while isinstance(gdb_value, gdb.Value):
        pretty_printer = gdb.default_visualizer(gdb_value)
        if pretty_printer is None:
            break
        if not hasattr(pretty_printer, 'to_string'):
            break
        gdb_value = pretty_printer.to_string()

    if isinstance(gdb_value, str):
        return gdb_value

    gdb_type = gdb_value.type
    is_string = gdb_type.code in (gdb.TYPE_CODE_ARRAY, gdb.TYPE_CODE_PTR) \
                and gdb_type.target().code == gdb.TYPE_CODE_INT and gdb_type.target().sizeof == 1

    if is_string:
        # the gdb.Value may not have an address or its invalid
        try:
            str_value = gdb_value.string('utf-8')
        except (gdb.MemoryError, OverflowError):
            str_value = 'NULL'
        return str_value

    if gdb_type.code == gdb.TYPE_CODE_ENUM:
        return str(gdb_value)
    if gdb_type.code == gdb.TYPE_CODE_INT:
        return int(gdb_value)
    if gdb_type.code == gdb.TYPE_CODE_FLT:
        return float(gdb_value)
    if gdb_type.code == gdb.TYPE_CODE_BOOL:
        return bool(gdb_value)
    if gdb_type.code == gdb.TYPE_CODE_REF:
        return to_python_value(gdb_value.referenced_value())
    if gdb_type.code == gdb.TYPE_CODE_ARRAY:
        return [to_python_value(gdb_value[idx]) for idx in range(*gdb_type.range())]
    if gdb_type.code == gdb.TYPE_CODE_STRUCT:

        def get_value(g_value):
            pretty = gdb.default_visualizer(g_value)
            if pretty is None:
                return to_python_value(g_value)
            elif hasattr(pretty, 'children'):
                return tuples_to_list(pretty.children())
            elif hasattr(pretty, 'to_string'):
                return pretty.to_string()

        return {name: get_value(gdb_value[field]) for name, field in
                gdb.types.deep_items(gdb_type) if
                not field.is_base_class}
    return gdb_value


def tuples_to_list(children):
    if children is None:
        return None
    if children is []:
        return []
    inner_list = []
    for _, value in children:
        if isinstance(value, gdb.Value):
            inner_val = gdb.default_visualizer(value)
            if inner_val is None:
                ss = to_python_value(value)
                # breakpoint()
                inner_list.append(ss)
            elif hasattr(inner_val, "children"):
                inner_list.append(tuples_to_list(inner_val.children()))
            else:
                to_string_val = inner_val.to_string()
                if isinstance(to_string_val, gdb.Value):
                    inner_list.append(to_python_value(to_string_val))
                else:
                    inner_list.append(inner_val.to_string())
        else:
            inner_list.append(value)
    return inner_list
