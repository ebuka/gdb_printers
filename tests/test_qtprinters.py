import inspect
import os
import re
import sys
import unittest
from unittest import TestSuite, TestLoader

from printers import register_qt_printers
from test_helpers import (stop_after_init_function, stop_after_end_function,
                          PrettyPrinterTest, tuples_to_list, to_python_value)


def run_printer_tests(module_contents):
    """Scan through module_contents (Iterable[Any]) and run unit tests from all PrettyPrinterTest subclasses
        matching to environment variable TEST_REGEX"""
    test_re_str = os.environ.get('TEST_REGEX', '.*')
    test_re = re.compile(test_re_str)
    test_cases = [obj for obj in module_contents
                  if inspect.isclass(obj)
                  and issubclass(obj, PrettyPrinterTest)
                  and obj is not PrettyPrinterTest
                  and test_re.search(obj.__name__)]
    test_cases.sort(key=lambda case: case.__name__)

    suite = TestSuite(TestLoader().loadTestsFromTestCase(test_case) for test_case in test_cases)
    return unittest.TextTestRunner(verbosity=2).run(suite)


class QAtomicIntegerTest(PrettyPrinterTest):
    func_name = 'test_qatomic'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('atomicShort', 0, None, None)

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('atomicInt', 500000, None, None)

    def test_long_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('atomicLongLong', 1234567890101112, None, None)


class QBitArrayPrinterTest(PrettyPrinterTest):
    func_name = 'test_qbitarray'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('bitArrayInit', '<empty>', 'string', None)

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('mixedArray', '<5 items>', 'array', [1, 0, 1, 0, 1])

    def test_false_bit_array(self):
        stop_after_end_function(self.func_name)
        self.check_equals('falseArray', '<5 items>', 'array', [0, 0, 0, 0, 0])

    def test_true_bit_array(self):
        stop_after_end_function(self.func_name)
        self.check_equals('trueArray', '<5 items>', 'array', [1, 1, 1, 1, 1])

    def test_cleared_bit_array(self):
        stop_after_end_function(self.func_name)
        self.check_equals('emptyArray', '<empty>', 'string', None)

    def test_resized_bit_array(self):
        stop_after_end_function(self.func_name)
        # when resized the bit are assigned to false
        self.check_equals('bitArrayInit', '<10 items>', 'array', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

    def test_truncated_bit_array(self):
        stop_after_end_function(self.func_name)
        self.check_equals('trunArray', '<5 items>', 'array', [1, 0, 1, 0, 1])


class QByteArrayPrinterTest(PrettyPrinterTest):
    func_name = 'test_qbytearray'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('byteArray', '', 'string', [])

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('byteArray', 'Hello There', 'string', [72, 101, 108, 108, 111, 32, 84, 104, 101, 114, 101])
        self.check_equals('anotherArray', 'General Kenobi hmm', 'string',
                          [71, 101, 110, 101, 114, 97, 108, 32, 75, 101, 110, 111, 98, 105, 32, 104, 109, 109])


class QCharPrinterTest(PrettyPrinterTest):
    FUNC_NAME = 'test_qchar'

    def test_variables_assigned(self):
        stop_after_end_function(self.FUNC_NAME)
        self.check_equals('letter', "97 'a'", None, None)


class QDirPrinterTest(PrettyPrinterTest):
    func_name = 'test_files'
    test_dir_path = os.path.join(os.getcwd(), 'tests', 'cpp_src_test', 'test_dir')

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)

        string, display_hint, _ = self.get_printer_result('buildDir')
        # check the to_string
        self.assertEqual(to_python_value(string), self.test_dir_path.replace("\\", "/"))
        self.assertEqual(display_hint, 'array')

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        string, display_hint, _ = self.get_printer_result('innerDir')
        current_path = os.path.join(self.test_dir_path, 'inner_dir')

        self.assertEqual(to_python_value(string), current_path.replace("\\", '/'))
        self.assertEqual(display_hint, 'array')


class QFilePrinterTest(PrettyPrinterTest):
    func_name = 'test_files'
    test_dir_path = os.path.join(os.getcwd(), 'tests', 'cpp_src_test', 'test_dir')

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        # init_file = os.path.join(self.test_dir_path, '')
        self.check_equals('initFile', "", 'string', None)

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        file_one = os.path.join(self.test_dir_path, 'file_one.txt')
        inner_one = os.path.join(self.test_dir_path, 'inner_dir', 'inner_one.py')

        self.check_equals('fileOne', file_one.replace("\\", "/"), 'string', None)
        self.check_equals('innerOne', inner_one.replace("\\", "/"), 'string', None)


class QFileInfoPrinterTest(PrettyPrinterTest):
    func_name = 'test_files'
    test_dir_path = os.path.join(os.getcwd(), 'tests', 'cpp_src_test', 'test_dir')

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('initFileInfo', "", 'array', ["", "", "", "", ""])

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        file_one_info = os.path.join(self.test_dir_path, 'file_one.txt').replace("\\", "/")
        inner_one_info = os.path.join(self.test_dir_path, 'inner_dir', 'inner_one.py').replace("\\", "/")

        # TODO use file info with canonical path
        self.check_equals('fileOneInfo', file_one_info, 'array',
                          [os.path.dirname(file_one_info), file_one_info, os.path.dirname(file_one_info),
                           file_one_info, os.path.splitext(os.path.basename(file_one_info))[0]])
        self.check_equals('innerOneInfo', inner_one_info, 'array',
                          [os.path.dirname(inner_one_info), inner_one_info, os.path.dirname(inner_one_info),
                           inner_one_info, os.path.splitext(os.path.basename(inner_one_info))[0]])


class QHashPrinterTest(PrettyPrinterTest):
    func_name = 'test_qhash'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('hash', '<empty>', 'map', [])

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        string, display_hint, children = self.get_printer_result('hash')
        self.assertEqual(string, '<4 items>')
        self.assertEqual(display_hint, 'map')
        self.assertEqual(set(tuples_to_list(children)), {'one', 1, 'two', 2, 'three', 3, 'four', 4})


class QLinkedListPrinterTest(PrettyPrinterTest):
    func_name = 'test_qlinked_list'
    display_hint = 'array'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('linkedList', '<empty>', self.display_hint, [])

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('linkedList', '<4 items>', self.display_hint, [
            "one", "two", "three", "four"
        ])

    def test_embedded_qlist(self):
        stop_after_end_function(self.func_name)
        self.check_equals('embedQList', '<3 items>', self.display_hint, [
            [1, 2, 3, 4, 5],
            [10, 20, 30, 40, 50],
            [9, 8, 7, 6, 5]
        ])

    def test_embedded_qlinked_list(self):
        stop_after_end_function(self.func_name)
        self.check_equals('embedQLinkedList', "<3 items>", self.display_hint, [
            ["one", "two", "three", "four"],
            ["apple", "orange", "mango", "guava"],
            ["pencil", "pen", "book", "paper"]
        ])


class QListPrinterTest(PrettyPrinterTest):
    func_name = 'test_qlist'
    display_hint = 'array'

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        string, display_hint, children = self.get_printer_result('charL')
        self.assertEqual(string, '<2 items>')
        self.assertEqual(display_hint, self.display_hint)
        self.assertEqual(tuples_to_list(children), [["113 'q'", "114 'r'", "115 's'", "102 'f'", "102 'f'", "117 'u'"],
                                                    ["97 'a'", "98 'b'", "99 'c'", "100 'd'", "51 '3'", "42 '*'"]])

    def test_nested_list(self):
        string1, display_hint1, children1 = self.get_printer_result('listPPair')
        self.assertEqual(string1, '<2 items>')
        self.assertEqual(display_hint1, self.display_hint)
        self.assertEqual(tuples_to_list(children1), [
            {'first': 23, 'second': {
                'first': 12, 'second': 'this is a pair'
            }
             },
            {'first': 23, 'second': {
                'first': 12, 'second': 'this is a pair'
            }
             }
        ])

    def test_small_type_list(self):
        stop_after_end_function(self.func_name)
        string, display_hint, children = self.get_printer_result('smallList')
        self.assertEqual(string, '<3 items>')
        self.assertEqual(display_hint, self.display_hint)
        self.assertEqual(tuples_to_list(children), [{'val': 1}, {'val': 324}, {'val': 3000}])

    def test_large_type_list(self):
        stop_after_end_function(self.func_name)
        string, display_hint, children = self.get_printer_result('largeList')
        self.assertEqual(string, '<2 items>')
        self.assertEqual(display_hint, self.display_hint)
        self.assertEqual(tuples_to_list(children), [{'val1': 3234, 'val2': 3235, 'val3': 3236, 'val4': 3237},
                                                    {'val1': 912, 'val2': 913, 'val3': 914, 'val4': 915}])

    def test_qstring_list(self):
        stop_after_end_function(self.func_name)
        string, display_hint, children = self.get_printer_result("stringList")
        self.assertEqual(string, '<5 items>')
        self.assertEqual(display_hint, self.display_hint)
        self.assertEqual(tuples_to_list(children), ['one', 'two', 'three', 'four', 'five'])


class QMapPrinterTest(PrettyPrinterTest):
    func_name = 'test_qmap'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('qMapInit', '<empty>', 'map', [])
        self.check_equals('qMultiMap', '<empty>', 'map', [])

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('qMapInit', '<4 items>', 'map', [1, "one", 2, "two", 3, "three", 4, "four"])

        v_string, v_display_hint, v_children = self.get_printer_result('variantMap')
        self.assertEqual(v_string, '<3 items>')
        self.assertEqual(v_display_hint, 'map')
        self.assertEqual(set(tuples_to_list(v_children)),
                         {"dog", '"corgi" (QString)', "thin", 'false (bool)', "length", "32 (int)"})

    def test_qmulti_map(self):
        stop_after_end_function(self.func_name)
        self.check_equals('qMultiMap', '<3 items>', 'map', ['plenty', 5000, 'plenty', 2000, 'plenty', 100])


class QSetPrinterTest(PrettyPrinterTest):
    func_name = 'test_qset'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('qset', '<empty>', 'string', None)

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        string, display_hint, children = self.get_printer_result('qset')
        self.assertEqual(string, '<5 items>')
        self.assertEqual(display_hint, 'array')
        self.assertEqual(set(tuples_to_list(children)), {"one", "two", "three", "four", "five"})


class QStringPrinterTest(PrettyPrinterTest):
    FUNC_NAME = 'test_qstring'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.FUNC_NAME)
        self.check_equals('str', "", 'string', None)

    def test_variables_assigned(self):
        stop_after_end_function(self.FUNC_NAME)
        self.check_equals('str', "a new string", 'string', None)
        self.check_equals('fUtf8', "test utf8", 'string', None)


class QStringRefPrinterTest(PrettyPrinterTest):
    func_name = 'test_qstringRef'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('reference', 'NULL', 'string', None)

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('strRef', 'is a string', 'array', [
            'is a string', 'this is a string ref', 5, 11
        ])
        self.check_equals('reference', 'this is a string ref', 'array',
                          ['this is a string ref', 'this is a string ref', 0, 20])


class QTimePrinterTest(PrettyPrinterTest):
    func_name = 'test_qtime'
    display_hint = None

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('time', "<invalid>", self.display_hint, None)

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('time', "10:20:30.004", self.display_hint, None)
        self.check_equals('newDay', "00:00:00.000", self.display_hint, None)
        self.check_equals('endDay', "23:59:59.999", self.display_hint, None)
        self.check_equals('wrongDay', "<invalid>", self.display_hint, None)


class QTimeZonePrinterTest(PrettyPrinterTest):
    func_name = 'test_qtime'
    display_hint = 'string'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('timeZone', 'NULL', self.display_hint, None)

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('utcPlus1', 'UTC+01:00', self.display_hint, None)

    def test_out_of_range_offset(self):
        stop_after_end_function(self.func_name)
        self.check_equals('outOfRangeTZone', 'NULL', self.display_hint, None)
        self.check_equals('outOfRangeTZoneNeg', 'NULL', self.display_hint, None)


class QUuidPrinterTest(PrettyPrinterTest):
    func_name = 'test_quuid'
    display_hint = 'string'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('uuidInit', '00000000-0000-0000-0000-000000000000', self.display_hint, None)

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('assignedUuid', '67c8770b-44f1-410a-ab9a-f9b5446f13ee', self.display_hint, None)

    def test_random_string_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('fromRandomString', '00000000-0000-0000-0000-000000000000', self.display_hint, None)


class QUrlPrinterTest(PrettyPrinterTest):
    func_name = 'test_qurl'
    display_hint = 'string'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('url', "<uninitialized>", self.display_hint, None)
        self.check_equals('link', "<uninitialized>", self.display_hint, None)

    def test_variables_assigned(self):
        stop_after_end_function(self.func_name)
        self.check_equals('url', "/home/da-viper/Documents", self.display_hint, None)
        self.check_equals('fileRelative', "qml/main.qml", self.display_hint, None)
        self.check_equals('link', "http://localhost:1313/", self.display_hint, None)
        self.check_equals('mailTo', "mailto:jsmith@example.com?subject=A Test body=My idea is", self.display_hint,
                          None)
        self.check_equals('file', "/home/User/Document/test", self.display_hint, None)
        # does not include passwords when printing the url
        self.check_equals('ftpLink', "ftp://user@server/pathname", self.display_hint, None)


class QVariantPrinterTest(PrettyPrinterTest):
    func_name = 'test_qvariant'
    display_hint = None

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        # self.check_equals('charVariant', '', self.display_hint, None)
        self.check_equals('variantInit', "QVariant::Null (Invalid)", self.display_hint, None)

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('charVariant', "97 'a' (QChar)", self.display_hint, None)

    def test_primitives_variant(self):
        stop_after_end_function(self.func_name)
        self.check_equals('dVar', "324.5 (double)", self.display_hint, None)
        self.check_equals('rVar', "4234 (double)", self.display_hint, None)  # Qreal
        self.check_equals('llVar', "3234242324432 (qlonglong)", self.display_hint, None)
        self.check_equals('intVariant', "200 (int)", self.display_hint, None)

    def test_null_variant(self):
        stop_after_end_function(self.func_name)
        self.check_equals('nullVariant', "QVariant::Null (std::nullptr_t)", self.display_hint, None)
        self.check_equals('setToNull', "QVariant::Null (std::nullptr_t)", self.display_hint, None)

    def test_print_as_string(self):
        stop_after_end_function(self.func_name)
        self.check_equals('strVariant', '"this is a string" (QString)', self.display_hint, None)
        self.check_equals('urlVariant', '"https://www.example.com" (QUrl)', self.display_hint, None)
        self.check_equals('easingCurveVariant', "QEasingCurve::BezierSpline (QEasingCurve)", self.display_hint, None)

    def test_print_as_children(self):
        stop_after_end_function(self.func_name)
        self.check_equals('sListVariant', "QStringList", 'array',
                          ["QVariant::StringList", ["one", "two", "three", "four"]])

    def test_qvariant_from_value(self):
        stop_after_end_function(self.func_name)
        string, display_hint, children = self.get_printer_result('hashVar')
        self.assertEqual(string, "QHash<int,QString>")
        self.assertEqual(display_hint, 'array')

        # since the values are not ordered in a qhash, a set is used here for the types
        children = tuples_to_list(children)
        # NOTE: since there may be custom user types it does not always show the enum name.
        # self.assertEqual(children[0], 'QVariant::UserType')
        self.assertEqual(set(children[1]), {1, "one", 2, "two", 3, "three"})

    def test_not_pretty_printed_type(self):
        # """test for classes that does not need a pretty printer"""
        stop_after_end_function(self.func_name)
        self.check_equals('sizeVariant', 'QSize', 'array', ['QVariant::Size', {"wd": 10, "ht": 30}])

    def test_shared_type(self):
        stop_after_end_function(self.func_name)
        self.check_equals('uuidVar', '"67c8770b-44f1-410a-ab9a-f9b5446f13ee" (QUuid)', self.display_hint, None)


class QVectorPrinterTest(PrettyPrinterTest):
    func_name = 'test_qvector'

    def test_initialized_with_no_variables(self):
        stop_after_init_function(self.func_name)
        self.check_equals('qVectorInit', '<empty>', 'array', [])
        self.check_equals('embedQList', '<empty>', 'array', [])

    def test_assigned_variables(self):
        stop_after_end_function(self.func_name)
        self.check_equals('qVectorInit', '<5 items>', 'array', [1, 2, 3, 4, 5])
        self.check_equals('stringVector', '<5 items>', 'array', ["this", "is", "a", "QString", "vector"])

    def test_template_vector(self):
        stop_after_end_function(self.func_name)
        self.check_equals('embedQList', '<2 items>', 'array', [[10, 20, 30, 40], [1, 1, 1, 1]])

    def test_embedded_vector(self):
        stop_after_end_function(self.func_name)
        self.check_equals('embeddedVector', '<2 items>', 'array',
                          [["this", "is", "a", "QString", "vector"], ["this", "is", "a", "QString", "vector"]])


print("----------------------------------------------------------------------------")
print("")
print('[INFO][GDB_VERSION] GDB version: %s' % gdb.VERSION)
print('[INFO][GDB_PYTHON_VERSION] Python version: %s' % sys.version)
print("")
print("----------------------------------------------------------------------------")
print('[INFO][QT_PRINTERS]: Registering QT Printers ***')
register_qt_printers(None)
print('[INFO][QT_PRINTERS]: Running unit tests')
test_result = run_printer_tests(globals().values())
print("[INFO][QT_PRINTERS]: Finished test")
sys.exit(0 if test_result.wasSuccessful() else 1)
