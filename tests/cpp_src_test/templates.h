#include <utility>

//
// Created by da-viper on 21/06/2021.
//

#ifndef TESTQSTRING_ANIMAL_H
#define TESTQSTRING_ANIMAL_H

class Animal: public QObject
{

public:
	explicit Animal(QString &name, int size, QString &habitat, QObject *parent = nullptr);
	~Animal() override;
private:
	QString animalName;
	QString ssName;
	int size;
	QString habitat;
};

Animal::Animal(QString &name, int size, QString &habitat, QObject *parent)
	: QObject(parent), animalName(name), size(size), habitat(habitat)
{
	ssName = animalName + "ssName";
}

class CustomSmall
{

public:
	explicit CustomSmall(short v)
		: val(v)
	{}
	short val;
};

class CustomLarge
{

public:
	explicit CustomLarge(quint64 val)
		:
		val1(val),
		val2(val + 1),
		val3(val + 2),
		val4(val + 3)
	{
	}

	quint64 val1;
	quint64 val2;
	quint64 val3;
	quint64 val4;

};

Animal::~Animal() = default;
#endif //TESTQSTRING_ANIMAL_H
