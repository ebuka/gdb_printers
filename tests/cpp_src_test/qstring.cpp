#include <QString>
#include <QList>
#include <QVariant>
#include <QUrl>
#include <QTime>
#include <QPair>
#include <QDir>
#include <QSet>
#include <QUuid>
#include <QEasingCurve>
#include <QJsonValue>
#include <QSizePolicy>
#include <QCoreApplication>
#include <QBitArray>
#include <QSize>
#include <QLinkedList>
#include <QTimeZone>

#include "templates.h"

//add when variables has been initialized
void init_breakpoint()
{
}

// add at the end of a function
void end_breakpoint()
{
}

void test_qatomic()
{
	QAtomicInteger<short> atomicShort;
	init_breakpoint();

	atomicShort = 2345;
	QAtomicInt atomicInt(500000);
	QAtomicInteger<long long> atomicLongLong(1234567890101112);

	end_breakpoint();
}
void test_qbitarray()
{
	QBitArray bitArrayInit;
	init_breakpoint();

	QBitArray falseArray(5, false);
	QBitArray trueArray(5, true);

	QBitArray mixedArray(5);
	mixedArray.setBit(0, true);
	mixedArray.setBit(1, false);
	mixedArray.setBit(2, true);
	mixedArray.setBit(3, false);
	mixedArray.setBit(4, true);

	QBitArray trunArray(7);
	trunArray.setBit(0, true);
	trunArray.setBit(1, false);
	trunArray.setBit(2, true);
	trunArray.setBit(3, false);
	trunArray.setBit(4, true);
	trunArray.setBit(5, false);
	trunArray.setBit(6, true);

	trunArray.truncate(5);
	bitArrayInit.resize(10);

	QBitArray emptyArray(5, false);
	emptyArray.clear();
	end_breakpoint();
}

void test_qstring()
{
	QString str;
	init_breakpoint();
	QString fUtf8 = QString::fromUtf8("\x74\x65\x73\x74\x20\x75\x74\x66\x38"); //test utf8
	str = "a new string";

	end_breakpoint();
}

void test_qchar()
{
	QChar noInit;
	init_breakpoint();

	QChar letter('a');
	QChar specialChar(QChar::Space);
	QChar newLine('\n');
	QChar ss(QChar::Nbsp);

	end_breakpoint();
}

void test_qmap()
{
	QMap<int, QString> qMapInit;
	QMultiMap<QString, int> qMultiMap;
	init_breakpoint();

	qMapInit.insert(1, "one");
	qMapInit.insert(2, "two");
	qMapInit.insert(3, "three");
	qMapInit.insert(4, "four");

	QVariantMap variantMap;
	variantMap.insert("dog", "corgi");
	variantMap.insert("thin", false);
	variantMap.insert("length", 32);

	QMultiMap<QString, int> map1, map2;

	map1.insert("plenty", 100);
	map1.insert("plenty", 2000);
	// map1.size() == 2

	map2.insert("plenty", 5000);
	// map2.size() == 1

	qMultiMap = map1 + map2;
	// map3.size() == 3
	end_breakpoint();

}

void test_qurl()
{
	QUrl url;
	QUrl link;
	init_breakpoint();

	url.setPath("/home/da-viper/Documents");
	QUrl fileRelative("qml/main.qml");
	link.setPath("http://localhost:1313/");
	QUrl mailTo("mailto:jsmith@example.com?subject=A%20Test%20body=My%20idea%20is");
	QUrl file("/home/User/Document/test");
	QUrl ftpLink("ftp://user:password@server/pathname");

	end_breakpoint();
}

void test_qtime()
{
	QTime time;
	QTimeZone timeZone;
	init_breakpoint();

	time.setHMS(10, 20, 30, 4);
	QTime endDay(23, 59, 59, 999);
	QTime newDay(0, 0, 0, 0);
	QTime wrongDay(10, 333, 4, 23);

	QTimeZone utcPlus1(3600);
	QTimeZone outOfRangeTZone(500000);
	QTimeZone outOfRangeTZoneNeg(-500000);

	end_breakpoint();
}

void test_qlinked_list()
{
	QLinkedList<QString> linkedList;

	init_breakpoint();
	linkedList.append("one");
	linkedList.append("two");
	linkedList.append("three");
	linkedList.append("four");

	QLinkedList<QList<int>> embedQList{
		{1, 2, 3, 4, 5},
		{10, 20, 30, 40, 50},
		{9, 8, 7, 6, 5}
	};

	QLinkedList<QLinkedList<QString>> embedQLinkedList{
		{"one", "two", "three", "four"},
		{"apple", "orange", "mango", "guava"},
		{"pencil", "pen", "book", "paper"}
	};

	end_breakpoint();
}
void test_qlist()
{
	QList<QString> slist;
	QList<QChar> clist;
	QList<QVariant> sdsf;
	QList<QPair<int, int>> plist;
	init_breakpoint();

	QList<CustomSmall> smallList{
		CustomSmall(1), CustomSmall(324), CustomSmall(3000)
	};
	QList<CustomLarge> largeList{
		CustomLarge(3234), CustomLarge(912)
	};
	QPair<int, QString> pair(12, "this is a pair");
	QPair<int, QPair<int, QString>> qp(23, pair);
	QList<QPair<int, QPair<int, QString>>> listPPair{
		qp, qp
	};
	QStringList stringList{
		QString("one"),
		QString("two"),
		"three",
		"four",
		"five",
	};
	QList<QList<QChar>> charL = {{'q', 'r', 's', 'f', 'f', 'u'},
								 {'a', 'b', 'c', 'd', '3', 42}};

	end_breakpoint();
}

void test_qbytearray()
{
	QByteArray byteArray;
	init_breakpoint();
	byteArray = "Hello There";
	QByteArray anotherArray("General Kenobi");
	anotherArray.append(" hmm");

	end_breakpoint();
}

void test_qstringRef()
{
	QString ssfdf("shsfda");
	QString string("this is a string ref");
	QStringRef reference;
	init_breakpoint();
	reference = &string;
	QStringRef strRef(&string, 5, 11);
	end_breakpoint();
}

void test_qset()
{
	QSet<QString> qset;
	init_breakpoint();
	qset.insert("one");
	qset.insert("two");
	qset.insert("three");
	qset.insert("four");
	qset.insert("five");
	end_breakpoint();
}

void test_qhash()
{
	QHash<QString, int> hash;
	init_breakpoint();
	hash.insert("one", 1);
	hash.insert("two", 2);
	hash.insert("three", 3);
	hash.insert("four", 4);
	end_breakpoint();
}

void test_qsizepolicy()
{

	QSizePolicy apolicy;
	init_breakpoint();
	QSizePolicy policy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
	QSizePolicy policy2(QSizePolicy::Maximum, QSizePolicy::Preferred);
	end_breakpoint();
}

void test_files()
{
	QDir buildDir(QCoreApplication::applicationDirPath());
	buildDir.cdUp();
	QString testDir("test_dir");
	buildDir.cd(testDir);
	QFile initFile;
	QFileInfo initFileInfo;
	buildDir.setFilter(QDir::NoDotAndDotDot | QDir::AllEntries);
	init_breakpoint();

	// Ignore dot and dotdot for test to be consistent accross platforms
	auto dirFileList = buildDir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries);
	auto dirFileInfoList = buildDir.entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries);

	QFile fileOne(buildDir.filePath("file_one.txt"));
	QFileInfo fileOneInfo(buildDir.filePath("file_one.txt"));

	buildDir.cd(QString("inner_dir"));
	QDir innerDir = buildDir.path(); // the inner dir in the test_dir dir
	QString innerFileName("inner_one.py");
	QFile innerOne(innerDir.filePath(innerFileName));
	QFileInfo innerOneInfo(innerDir.filePath(innerFileName));

	// testfor empty filelist
	auto emptyFileList = innerDir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries);
	auto emptyFileInfoList = innerDir.entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries);

	end_breakpoint();
//	QStandardPaths a
}

void test_quuid()
{
	QUuid uuidInit;
	init_breakpoint();

	QUuid fromRandomString = QUuid::fromString(QLatin1String("wsfdter"));
	QUuid assignedUuid(0x67c8770b, 0x44f1, 0x410a, 0xab, 0x9a, 0xf9, 0xb5, 0x44, 0x6f, 0x13, 0xee);

	end_breakpoint();
}

void test_qvariant()
{
	// TODO: add printers for the other types
	QVariant charVariant;
	QVariant variantInit;
	QVariant setToNull("this is to be set to null");
	QHash<int, QString> hash1;
	hash1.insert(1, "one");
	hash1.insert(2, "two");
	hash1.insert(3, "three");
	init_breakpoint();

	QVariant sizeVariant(QSize(10, 30));

	QVariant nullVariant = QVariant::fromValue(nullptr);
	setToNull.setValue(nullptr);

	charVariant = QChar('a');

	QVariant dVar((double)324.5);
	QVariant rVar((qreal)4234);
	QVariant llVar((long long)3234242324432);
	QVariant intVariant(200);

    QUuid muuid(0x67c8770b, 0x44f1, 0x410a, 0xab, 0x9a, 0xf9, 0xb5, 0x44, 0x6f, 0x13, 0xee);
	QVariant strVariant("this is a string");
	QVariant urlVariant(QUrl("https://www.example.com"));
	QVariant easingCurveVariant((QEasingCurve(QEasingCurve::BezierSpline)));
	QVariant uuidVar(QUuid(0x67c8770b, 0x44f1, 0x410a, 0xab, 0x9a, 0xf9, 0xb5, 0x44, 0x6f, 0x13, 0xee));
	QVariant jsonVar(QJsonValue("sttst"));

	QVariant sListVariant(QStringList{
		"one", "two", "three", "four"
	});
	QVariant bytearVar(QByteArray("/home/da-viper/Documents"));
	QVariant hashVar = QVariant::fromValue(hash1);

	end_breakpoint();

}

void test_qvector()
{
	QVector<int> qVectorInit;
	QVector<QList<int>> embedQList;
	QList<int> intList{10, 20, 30, 40};

	init_breakpoint();

	qVectorInit.append({1, 2, 3, 4, 5});
	QVector<QString> stringVector{"this", "is", "a", "QString", "vector"};

	QVector<QVector<QString>> embeddedVector{stringVector, stringVector};
	embedQList.append(intList);
	embedQList.append({1, 1, 1, 1});

	end_breakpoint();
}
int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	test_qatomic();
	test_qbitarray();
	test_qstring();
	test_qchar();
	test_qmap();
	test_qurl();
	test_qtime();
	test_qlinked_list();
	test_qlist();
	test_qbytearray();
	test_qstringRef();
	test_qset();
	test_qhash();
	test_files();
	test_quuid();
	test_qvariant();
	test_qvector();
	return 0;
}
