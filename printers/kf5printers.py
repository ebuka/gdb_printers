import gdb
from printers.helpers import split_struct, evaluate


class KConfigPrinter:
    def __init__(self, val):
        self.val_addr = val.address
        _, addr = split_struct("PP", self.val_addr)
        (void_ptr, e, _, _, _, _, _, _, _, _, d, c, b, a, self.fileName) = split_struct('PiiP??????PPPPP', addr)

    def to_string(self):
        return evaluate("(QString) %d" % self.fileName)


def build_kfcore_pretty_printer():
    """Builds the pretty printer for KF5core."""
    pp = gdb.printing.RegexpCollectionPrettyPrinter("KF5ConfigCore")

    pp.add_printer('KConfig', '^KConfig$', KConfigPrinter)

    return pp
