import struct
import sys

import gdb.printing

from printers.qt_types_info import movable_tpl_types, movable_types, primitive_types

if sys.version_info[0] == 3:
    long = int
else:
    range = xrange


def get_qt_version():
    """Get the version of QT installed

        this is only available when the debugged program has started
    """
    version = str(evaluate("(const char*) qVersion()"))
    version = version[version.find('"') + 1:version.rfind('"')]
    major, minor, patch = map(int, version.split('.'))

    return (major << 16) | (minor << 8) | patch


def _byte_order():
    b_order = sys.byteorder
    if b_order == 'little':
        return '<'
    if b_order == 'big':
        return '>'


def pointer_size():
    return lookup('void').pointer().sizeof


BYTE_ORDER = _byte_order()

"""True if the OS is windows """
IS_WINDOWS = 'mingw' in gdb.TARGET_CONFIG.lower()


def default_iterator(val):
    """Returns the origial fields in a gdb value """

    for field in val.type.fields():
        yield field.name, val[field.name]


class ArrayIter:
    """Iterates over a fixed-size array."""

    def __init__(self, array, size):
        self.array = array
        self.index = -1
        self.size = size

    def __iter__(self):
        return self

    def __next__(self):
        self.index += 1
        if self.index == self.size:  # to account for zero
            raise StopIteration

        return ("%d" % self.index, self.array[self.index])

    def next(self):
        return self.__next__()


class CborJsonIter:
    """Iterates over a fixed-size array."""

    def __init__(self, array_ptr, size, container_ptr):
        self.array_ptr = array_ptr
        self.index = -1
        self.size = size
        self.con_ptr = container_ptr

    def __iter__(self):
        return self

    def __next__(self):
        self.index += 1
        if self.index == self.size:  # to account for zero
            raise StopIteration

        return ("%d" % self.index, cbor_json_val_at(self.array_ptr, self.con_ptr, self.index))

    def next(self):
        return self.__next__()


def cbor_json_val_at(element_data_ptr, container_ptr, index):
    element_at_n_addr = element_data_ptr + index * 16  # sizeof(QtCbor::Element) == 15
    element_value, element_type, element_flags = split_struct('Pii', element_at_n_addr)
    if element_flags & 1:
        return qcbor_private_ptr_to_string(element_value, element_value, element_type)
    if element_flags & 2:
        return qcbor_private_ptr_to_string(index, container_ptr, element_type)
    return qcbor_private_ptr_to_string(element_value, 0, element_type)


class StructReader:
    """Reads entries from a struct."""

    # TODO: try to use memory view check if it is faster
    # with  gdb.selected_inferior().read_memory(address, size)
    def __init__(self, data):
        self.data = cast_ptr(data, 'char')
        self.ptr_t = lookup('void').pointer()

    def next_aligned_val(self, typ):
        # to base 16
        # Fixme: either pointer size or base 16
        ptr_val = int(str(self.data.reinterpret_cast(self.ptr_t)), 16)  # base 16
        misalignment = ptr_val % self.ptr_t.sizeof
        if misalignment > 0:
            self.data += self.ptr_t.sizeof - misalignment
        val = self.data.reinterpret_cast(typ.pointer())
        self.data += typ.sizeof
        return val.referenced_value()

    def next_val(self, variable_type):
        value = cast_ptr_type(self.data, variable_type)
        self.data += variable_type.sizeof
        return value.referenced_value()

    def add_to_ptr(self, val):
        self.data += val


def pointer_iter(val, tpl_type=None):
    """Returns the fields() values of a pointer"""

    data = val['d']
    value = val['value']

    # QPointer has it in its top class declaration
    if tpl_type is None:
        tpl_type = val.type.template_argument(0)
    else:
        tpl_type = tpl_type
    data_ptr = cast_ptr_type(value, tpl_type)

    yield ('weakref', data['weakref'])
    yield ('strongref', data['strongref'])
    yield ('data', data_ptr.dereference())


def cast_ptr(d, pointer_type, offset=0):
    return cast_ptr_type(d, lookup(pointer_type), offset)


def cast_ptr_type(d, pointer_type, offset=0):
    """ Casts a gdb Value to the gdb.Type pointer"""

    if offset != 0:
        return d.reinterpret_cast(pointer_type.pointer()) + offset
    return d.reinterpret_cast(pointer_type.pointer())


def evaluate(expression):
    """
    Converts the passed in expression to a gdb.Value type
    Args:
        expression (str): the expression to parse
    Returns:
        gdb.Value
    """
    return gdb.parse_and_eval(expression)


def run_method(class_name, var_address, method_name, method_args=""):
    """
    Converts a class method expression to a gdb.Value
    Args:
        class_name (str): the name of the class
        var_address (int): the class instance address in memory
        method_name (str): the method name
        method_args (str): the method arguments

    Returns:
        gdb.Value
    """
    return evaluate("reinterpret_cast<const %s*>(%d)->%s(%s)" % (class_name, var_address, method_name, method_args))


# gdb types dict
gdb_lookup_type_dict = {}


def lookup(type_name):
    """
    Convenience function to convert the passed type to gdb.Type
    the lookups are also cached in the lookup type dict
    Args:
        type_name (str): the name of the type
    Returns:
        gdb.Type
    """
    if type_name in gdb_lookup_type_dict:
        return gdb_lookup_type_dict[type_name]

    type_value = gdb.lookup_type(type_name)
    gdb_lookup_type_dict[type_name] = type_value

    return type_value


def format_milliseconds(millis):
    """Format a number of milliseconds since midnight in HH:MM:SS.ssss format."""
    secs = millis // 1000
    mins = secs // 60
    hours = mins // 60
    return "%02d:%02d:%02d.%03d" % (hours % 24, mins % 60, secs % 60, millis % 1000)


def ms_is_valid(msecs):
    """Return whether QTime would consider a ms since midnight valid."""
    return 0 <= msecs <= 86400000


def read_memory(address, size):
    """Gets the value of an address in bytes

    Args:
        address (hex): the address in memory
        size (int): the length of the data to read

    Returns:
        memoryview: the data that is read
    """
    if address == 0 or size == 0:
        return memoryview(bytes())
    return gdb.selected_inferior().read_memory(address, size)


def is_initialized(data_ptr):
    """Checks if the pointer of the class has been initialised"""
    try:
        # the actual type here is QAtomicInt but sometimes it is not
        # exist in the debugging symbols, it is when there is other classes like
        # QVariant so cast to int instead as that is always available.
        ref = int(str(cast_ptr(data_ptr, 'int').dereference()))
        init = ref == 1
    except gdb.MemoryError:
        init = False
    return init


def type_is_primitive(typ):
    """Returns True if the given gdb type is known to be primitive."""
    if typ.code == gdb.TYPE_CODE_PTR:
        return True
    else:
        return typ.name in primitive_types


def type_is_movable(gdb_type):
    """Returns True if the given gdb type is known to be movable."""
    if gdb_type.tag is None:
        if gdb_type.name is None:
            return False
        t_name = gdb_type.name
    else:
        t_name = gdb_type.tag

    pos = t_name.find('<')
    if pos > 0:
        return t_name[0:pos] in movable_tpl_types
        # for lists of lists eg QList<Qlist<QString>>
    else:
        return gdb_type.name in movable_types


def qt_type_info_version():
    """Returns: the installed QT typeInfo verison"""

    # Only available with Qt 5.3+
    hook_data_addr = int(evaluate('(size_t)&qtHookData'))
    pattern = 'QQQQQQQ'

    if hook_data_addr:
        mem_view = read_memory(hook_data_addr, struct.calcsize(pattern))
        (hook_version, _, _, _, _, _, ti_version) = struct.unpack_from(BYTE_ORDER + pattern, mem_view)
        if hook_version >= 3:
            return ti_version
    return None


def split_struct(pattern, address):
    """
    Unpacks a memory view to a tuple from a give pattern

    Args:
        pattern (str): the pattern to read in memory
        address (int): the address in memory

    Returns:
    """

    mem_view = read_memory(address, struct.calcsize(pattern))
    return struct.unpack_from(pattern, mem_view)


def qcbor_private_ptr_to_string(cbor_n, container_ptr, cbor_type):
    """Match the cbor type and print using the correct function call"""

    # from https://code.woboq.org/qt5/qtbase/src/corelib/serialization/qcborvalue.h.html#QCborValue::Type
    _enum_values_dict = {
        -1: lambda x, y, z: "<invalid>",
        0x00: lambda value, y, z: value,  # int
        0x40: _cbor_string_value,  # bytearray
        0x60: _cbor_string_value,  # string
        0x80: _cbor_json_array_value,  # array
        0xa0: _cbor_map_value,
        0x100 + 20: lambda x, y, z: True,
        0x100 + 21: lambda x, y, z: False,
        0x100 + 22: lambda x, y, z: "NULL",
        0x100 + 23: lambda x, y, z: "<Undefined>",
        0x202: _cbor_double_value,  # double
        0x10020: _cbor_string_value,  # url_value
    }
    return _enum_values_dict.get(cbor_type, lambda x, y, z: x)(cbor_n, container_ptr, cbor_type)


def _cbor_string_value(cbor_n, container_ptr, element_type):
    byte_data, byte_data_len = get_byte_data(cbor_n, container_ptr, element_type)
    result = byte_data.string('UTF-8', 'replace', length=byte_data_len)

    return result


def get_byte_data(cbor_n, container_ptr, element_type):
    # see toprint.md for container_ptr structure

    data_offset = 8  # sizeof QSharedData  usedData
    data_pos = container_ptr + data_offset
    elements_pos = data_pos + lookup('void').pointer().sizeof
    (vec_val,) = split_struct('P', elements_pos)  # get the referenced value
    _, vec_size, _, _, vec_offset = split_struct("iiiiq", vec_val)

    element_data_ptr = vec_val + vec_offset
    current_element_offset = cbor_n * 16 if element_type != 0x10020 else 16  # type array
    current_element_addr = element_data_ptr + current_element_offset  # sizeof(QtCbor::Element) == 16

    element_value, _, _ = split_struct('Pii', current_element_addr)
    byte_data_addr = evaluate("*(QByteArray *) %d" % data_pos)
    byte_data = byte_data_addr['d']
    byte_data = cast_ptr(byte_data, 'char', offset=(long(byte_data['offset'])))
    byte_data += element_value
    (byte_data_len,) = split_struct('i', byte_data)

    # # TODO: use the max print limit set in gdb
    byte_data += 4  # sizeof(QtCbor::ByteData) header part int
    return byte_data, byte_data_len


def _cbor_json_array_value(c_no, container_ptr, c_type):
    # because of the eay qcborarray is layed out you can cast a container_private
    # to cborarray unlike json_array
    return evaluate("(QCborArray) %d" % container_ptr)


def _cbor_map_value(c_no, container_ptr, c_type):
    return evaluate("(QCborMap) %d " % container_ptr)


def _cbor_double_value(c_no, w, x):
    (val,) = struct.unpack('d', struct.pack('q', c_no))

    return val


def get_vector_data(container_ptr):
    data_offset = 8  # sizeof QSharedData  usedData
    data_pos = container_ptr + data_offset
    elements_pos = data_pos + lookup('void').pointer().sizeof
    return split_struct('P', elements_pos)  # get the referenced value
