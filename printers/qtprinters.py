import itertools
import sys
import time

import gdb.printing

from printers.helpers import (cast_ptr, lookup, cast_ptr_type, ArrayIter, evaluate,
                              run_method, pointer_iter, format_milliseconds, ms_is_valid, StructReader,
                              is_initialized, type_is_movable, type_is_primitive, get_qt_version,
                              qt_type_info_version, IS_WINDOWS, pointer_size, split_struct, qcbor_private_ptr_to_string,
                              cbor_json_val_at, get_vector_data, default_iterator, CborJsonIter
                              )
from printers.qt_types_info import qimage_formats, meta_type_names

if sys.version_info[0] == 3:
    long = int

"""The current system endianness"""
SYS_ENDIANESS = sys.byteorder


def qs_to_str(q_string):
    """Converts a QString to a python string"""
    return QStringPrinter(q_string).to_string()


class QCborArrayPrinter:

    def __init__(self, val, con_type='d', is_map=False):
        self.val = val
        self.is_map = is_map

        # They all have QExplicitlySharedDataPointer that has different name
        # depending on the type
        self.container_ptr = long(self.val['%s' % con_type]['d'])

    def children(self):
        container_ptr = self.container_ptr
        (vec_val,) = get_vector_data(container_ptr)
        _, vec_size, _, _, vec_offset = split_struct("iiiiq", vec_val)
        elements_data_ptr = vec_val + vec_offset

        for index in range(vec_size):
            yield ("%d" % index, cbor_json_val_at(elements_data_ptr, container_ptr, index))

    def display_hint(self):
        if self.is_map:
            return 'map'
        return 'array'


class QCborValuePrinter:
    string_values = (
        -1,
        0x00,  # int
        0x40,  # bytearray
        0x60,  # string
        0x100 + 20,
        0x100 + 21,
        0x100 + 22,
        0x100 + 23,
        0x202,  # double
        0x10020,  # url_value
    )

    def __init__(self, val, is_json=False):
        self.val = val
        self.c_no = long(self.val['n'])
        self.c_type = long(self.val['t'])
        self.print_as_string = self.c_type in self.string_values
        self.is_json = is_json
        self.container_ptr = long(self.val['d']['d']) if self.is_json else long(self.val['container'])

        # this is the only way i know of to get around GDB/MI problem of
        # not printing the `to_string` if there is a `children` method
        if not self.print_as_string:
            setattr(self, 'children', self._children)

    def _children(self):
        value = self.val
        c_no = self.c_no
        container_ptr = self.container_ptr
        c_type = self.c_type
        if c_type == 0x80:  # type array
            if not container_ptr:
                return c_no

            (vec_val,) = get_vector_data(container_ptr)
            _, vec_size, _, _, vec_offset = split_struct("iiiiq", vec_val)
            elements_data_ptr = vec_val + vec_offset
            return CborJsonIter(elements_data_ptr, vec_size, container_ptr)
        else:
            return default_iterator(value)

    def display_hint(self):
        if self.print_as_string:
            return 'string'
        return 'array'

    def to_string(self):
        if self.print_as_string:
            return qcbor_private_ptr_to_string(self.c_no, self.container_ptr, self.c_type)
        return "QJsonValue" if self.is_json else "QCborValue"


class QCharPrinter:
    """Print a Qt5 QChar"""

    def __init__(self, val):
        self.val = val

    @staticmethod
    def display_hint():
        return None

    def to_string(self):
        ucs = self.val['ucs']
        return "%d '%s'" % (ucs, chr(ucs))


class QCommandLineOptionPrinter:

    def __init__(self, val):
        self.val = val
        self.val_private = val['d']['d']

    def children(self):
        reader = StructReader(self.val_private)
        int_type = lookup('int')
        shared_data = reader.next_val(int_type)
        qstring_type = lookup('QString')
        qstringlist_type = lookup('QStringList')
        names = reader.next_aligned_val(qstringlist_type)
        value_name = reader.next_val(qstring_type)
        description = reader.next_val(qstring_type)
        default_values = reader.next_val(qstringlist_type)
        flags = reader.next_val(lookup('QCommandLineOption::Flag'))

        yield ('names', names)
        yield ('valueName', value_name)
        yield ('description', description)
        yield ('defaultValues', default_values)
        yield ('flags', flags)

    def display_hint(self):
        return 'array'


class QAtomicIntegerPrinter:
    """Print a Qt5 AtomicInt"""

    def __init__(self, val):
        self.val = val

    def to_string(self):
        return self.val['_q_value']['_M_i']


class QStringPrinter:
    """Print a Qt5 QString"""

    def __init__(self, val):
        self.val = val

    @staticmethod
    def display_hint():
        return 'string'

    def to_string(self):
        try:
            d = self.val['d']
            data_size = long(d['size'])
            data = cast_ptr(d, 'char', offset=d['offset'])
            data_len = data_size * lookup('unsigned short').sizeof
            return data.string('utf-16', 'replace', length=data_len)
        except (gdb.MemoryError, OverflowError):
            return "<uninitialized>"


class QByteArrayPrinter:
    """Print a Qt5 QByteArray"""

    def __init__(self, val):
        data = val['d']
        self.size = long(data['size'])
        self.array = cast_ptr(data, 'char', offset=data['offset'])

    def children(self):
        return ArrayIter(self.array, self.size)

    @staticmethod
    def display_hint():
        return 'string'

    def to_string(self):
        return self.array.string('UTF-8', 'replace', self.size)


class QBitArrayPrinter:
    """Print a Qt5 QBitArray"""

    class Iter:
        def __init__(self, data, size):
            self.data = data
            self.index = -1
            self.size = size

        def __iter__(self):
            return self

        def __next__(self):
            # TODO: not sure if i should use false/true or 1/0
            self.index += 1

            if self.index >= self.size:
                raise StopIteration
            if self.data[1 + (self.index >> 3)] & (1 << (self.index & 7)):
                return (str(self.index), True)
            else:
                return (str(self.index), False)

        def next(self):
            return self.__next__()

    def __init__(self, val):
        self.val = val
        d = self.val['d']['d']
        self.data = cast_ptr(d, 'char', offset=d['offset'])
        self.size = (long(d['size']) << 3) - long(self.data[0])

        self.is_empty = self.size == 0
        if not self.is_empty:
            setattr(self, 'children', self._children)

    def _children(self):
        return self.Iter(self.data, self.size)

    def display_hint(self):
        if self.is_empty:
            return 'string'
        return 'array'

    def to_string(self):
        if self.is_empty:
            return '<empty>'
        return "<%d items>" % self.size


class QLinkedListPrinter:
    """Print a Qt5 QLinkedList"""

    class Iter:
        def __init__(self, head, size):
            self.current = head
            self.index = -1
            self.size = size

        def __iter__(self):
            return self

        def __next__(self):
            self.index += 1
            if self.index >= self.size:
                raise StopIteration

            self.current = self.current['n']  # next value
            value = self.current['t']

            return (str(self.index), value)

        def next(self):
            return self.__next__()

    def __init__(self, val):
        self.val = val
        self.size = self.val['d']['size']

    def children(self):
        if self.size == 0:
            return []

        return self.Iter(self.val['e'], self.size)

    @staticmethod
    def display_hint():
        return 'array'

    def to_string(self):
        # if we return an empty list from children, gdb doesn't print anything
        if self.size == 0:
            return '<empty>'
        return "<%d items>" % self.size


class QListPrinter:
    """Print a Qt5 QList"""

    class _Iterator:
        def __init__(self, array, begin, end, list_name, template_type):
            self.array = array
            self.end = end
            self.begin = begin
            self.index = -1
            self.t_type = template_type
            self.node_type = lookup(list_name.name + '::Node')

            # see https://doc.qt.io/qt-5/qlist.html#details
            not_large = self.t_type.sizeof <= lookup('void').pointer().sizeof

            self.is_array = not_large and (type_is_movable(self.t_type) or
                                           type_is_primitive(self.t_type))

        def __iter__(self):
            return self

        def __next__(self):
            self.index += 1
            current_item = self.begin + self.index

            if current_item >= self.end:
                raise StopIteration

            node = cast_ptr_type(self.array[current_item], self.node_type)
            value = node if self.is_array else node['v']

            return (str(self.index), value.cast(self.t_type))

        def next(self):
            return self.__next__()

    def __init__(self, val):
        self.val = val
        try:
            self.begin = long(self.val['d']['begin'])
            self.end = long(self.val['d']['end'])
            self.init = 0 <= self.begin and 0 <= self.end <= 1000 * 1000 * 1000
            self.size = self.end - self.begin
            self.init = self.init and (self.size >= 0)
        except gdb.error:
            self.init = False
        if self.init:
            setattr(self, 'children', self._children)

    def _children(self):
        if not self.init:
            return []

        d = self.val['d']
        val_type = self.val.type.strip_typedefs()

        if val_type.name == 'QStringList':
            t_type = lookup('QString')
        elif val_type.name == 'QVariantList':
            t_type = lookup('QVariant')
        elif val_type.name == 'QFileInfoList':
            t_type = lookup('QFileInfo')
        else:
            t_type = val_type.template_argument(0)

        return self._Iterator(d['array'], self.begin, self.end, val_type, t_type)

    def display_hint(self):
        if not self.init:
            return 'string'
        return 'array'

    def to_string(self):
        if not self.init:
            return '<uninitialized>'
        return "<%d items>" % self.size


class QVectorPrinter:
    """Print a Qt5 QVector"""

    def __init__(self, val):
        self.val = val
        # TODO: check if it is initialized
        self.size = long(val['d']['size'])
        self.is_empty = self.size < 1

    def children(self):
        d = self.val['d']
        tpl_type = self.val.type.template_argument(0)

        if self.is_empty:
            return []

        data_char_ptr = cast_ptr(d, 'char', offset=d['offset'])
        data = cast_ptr_type(data_char_ptr, tpl_type)

        return ArrayIter(data, self.size)

    @staticmethod
    def display_hint():
        return 'array'

    def to_string(self):
        if self.is_empty:
            return '<empty>'
        return "<%d items>" % self.size


class QStringRefPrinter:

    def __init__(self, val):
        self.val = val
        self.index = self.val['m_position']
        self.m_string_ptr = self.val['m_string']
        self.size = self.val['m_size']
        self.has_string = self.m_string_ptr != 0

        if self.has_string:
            setattr(self, 'children', self._children)

    def _children(self):
        if not self.has_string:
            return

        yield ('referenced', cast_ptr(gdb.Value(self.to_string()), 'char'))
        yield ('orginal_string', self.val['m_string'].referenced_value())
        yield ('position', self.index)
        yield ('size', self.size)

    def display_hint(self):
        if not self.has_string:
            return 'string'
        return 'array'

    def to_string(self):
        if not self.has_string:
            return 'NULL'
        try:
            d = self.m_string_ptr['d']
            ushort_size = lookup('unsigned short').sizeof
            data = cast_ptr(d, 'char', offset=d['offset']) + (self.index * ushort_size)
            data_len = int(self.size) * ushort_size
            return data.string('utf-16', 'replace', length=data_len)
        except (gdb.MemoryError, OverflowError):
            return "<uninitialized>"


class QVariantPrinter:
    _types_dict = {
        # these values are shown in gdb cli without printers
        34: 'c',  # char
        37: 'uc',  # unsigned char
        33: 's',  # short
        40: 'sc',  # signed char
        36: 'us',  # unsigned short
        2: 'i',  # int
        3: 'u',  # unsigned int
        32: 'l',  # long
        35: 'ul',  # unsigned long
        1: 'b',  # bool
        6: 'd',  # real is a type_def of double
        38: 'f',  # float
        4: 'll',  # long long
        5: 'ull',  # unsigned long long
        39: 'o',  # QObject
        31: 'ptr',  # void ptr
    }

    _has_children = {key: False for key in
                     (0, 1, 2, 3, 4, 5, 6, 7, 10, 15, 17, 29,
                      30, 31, 32, 33, 34, 36, 36, 37, 38, 40, 43,
                      44, 45, 51, 52, 53)}

    def __init__(self, val):
        self.val = val
        self.type = long(self.val['d']['type'])
        self.show_children = self._has_children.get(self.type, True)

        if self.show_children:
            setattr(self, 'children', self._children)

    def _children(self):
        yield ("type", gdb.Value(self.type).cast(lookup('QVariant::Type')))
        yield ("value", self.variant_value()[0])

    def display_hint(self):
        if self.show_children:
            return 'array'
        return None

    def to_string(self):
        type_name = self.get_type_name(self.type)

        if self.show_children:
            return type_name
        return "%s (%s)" % self.variant_value()

    def variant_value(self):
        d = self.val['d']
        type_no = long(d['type'])
        type_name = self.get_type_name(type_no)

        if d['is_null'] or type_no == 51:  # std::nullptr
            return ('QVariant::Null', type_name)

        # see https://doc.qt.io/qt-5/qmetatype.html#Type-enum
        if type_no == 0:  # metatype invalid
            return (lookup('QVariant::Invalid'), type_name)

        data = d['data']
        if type_no in self._types_dict:
            type_field = self._types_dict[type_no]
            return (data[type_field], type_name)

        try:
            gdb_type = gdb.lookup_type(type_name)
        except gdb.error:
            return (data['ptr'], type_name)

        is_shared = d['is_shared']
        if is_shared:
            value = cast_ptr_type(data['shared']['ptr'], gdb_type)
            return value.referenced_value(), type_name

        value = cast_ptr_type(data['c'].address, gdb_type)
        return (value.referenced_value(), type_name)

    @staticmethod
    def get_type_name(type_no):
        try:
            type_name = meta_type_names[type_no]
        except KeyError:
            # if it a custom typename
            type_name_value = cast_ptr(evaluate("QVariant::typeToName(%d)" % type_no), 'char')
            type_name = type_name_value.string(encoding='UTF-8')
        return type_name


class QDirPrinter:
    """Prints a Qt5 QDir"""

    def __init__(self, val):
        self.val = val
        self.private_ptr = self.val['d_ptr']['d']
        self.item = 0
        # evaluates directory initialization
        self.init = is_initialized(self.private_ptr)

        init_files_ptr = cast_ptr(self.private_ptr, 'bool', offset=4)
        init_files = init_files_ptr.dereference()
        file_init = str(init_files) == 'true'

        # fills the entrylist and entryinfolist cache if not there
        if not file_init and self.init:
            run_method('QDir', self.val.address, 'count')

    def __iter__(self):
        return self

    def __next__(self):
        # if self.is_initialized:
        if self.init:
            item = self.item
            self.item += 1

            if item > 2:
                raise StopIteration
            if item == 0:
                absolute_path = run_method('QDir', self.val.address, 'absolutePath')
                return ('absolute path', absolute_path)

            if item == 1:
                char_ptr = cast_ptr(self.private_ptr, 'void', offset=0x08)
                f_list_ptr = cast_ptr(char_ptr, 'QStringList')
                return ('entry list', f_list_ptr.dereference())

            if item == 2:
                char_ptr = cast_ptr(self.private_ptr, 'void', offset=0x10)
                finfo_list_ptr = cast_ptr(char_ptr, 'QFileInfoList')
                return ('entryinfo list', finfo_list_ptr.dereference())

        raise StopIteration

    def next(self):
        return self.__next__()

    def children(self):
        return self

    @staticmethod
    def display_hint():
        return 'array'

    def to_string(self):
        if not self.init:
            return '<uninitialized>'
        return run_method('QDir', self.val.address, 'absolutePath')


class QEventPrinter:

    def __init__(self, val):
        self.val = val
        _, _, self.val_type = split_struct('PPH', self.val.address)

    def to_string(self):
        return gdb.Value(self.val_type).cast(lookup("QEvent::Type"))


class QFileInfoPrinter:
    """Print a Qt5 QFileInfo """

    # none of the paths except the file path
    # are stored in memory have to call all the functions
    def __init__(self, val):

        self.val = val['d_ptr']['d']
        self.val_ref = long(self.val.address)
        val_mem_value = long(self.val)
        (ref,) = split_struct('i', val_mem_value)
        self.init = ref == 1
        ptr_size = lookup('void').pointer().sizeof
        self.file_path = evaluate("*(const QString*) %d" % (val_mem_value + ptr_size))
        if self.init:
            setattr(self, 'children', self._children)

    def _children(self):
        qclass_name = 'QFileInfo'
        val_ref = self.val_ref
        if self.init:
            yield ('absolutePath', run_method(qclass_name, val_ref, 'absolutePath'))
            yield ('absoluteFilePath', run_method(qclass_name, val_ref, 'absoluteFilePath'))
            yield ('canonicalPath', run_method(qclass_name, val_ref, 'canonicalPath'))
            yield ('canonicalFilePath', run_method(qclass_name, val_ref, 'canonicalFilePath'))
            yield ('completeBaseName', run_method(qclass_name, val_ref, 'completeBaseName'))

    def display_hint(self):
        return 'array'

    def to_string(self):
        return self.file_path


class QMapPrinter:
    """Print a Qt5 QMap"""

    class Iter:
        def __init__(self, root, node_p_type):
            self.root = root
            self.current = None
            self.node_p_type = node_p_type
            self.next_is_key = True
            self.index = -1
            # we store the path here to avoid keeping re-fetching
            # values from the inferior (also, skips the pointer
            # arithmetic involved in using the parent pointer)
            self.path = []

        def __iter__(self):
            return self

        def move_to_next_node(self):
            if self.current is None:
                # find the leftmost node
                if not self.root['left']:
                    return False
                self.current = self.root
                while self.current['left']:
                    self.path.append(self.current)
                    self.current = self.current['left']
            elif self.current['right']:
                self.path.append(self.current)
                self.current = self.current['right']
                while self.current['left']:
                    self.path.append(self.current)
                    self.current = self.current['left']
            else:
                last = self.current
                self.current = self.path.pop()
                while self.current['right'] == last:
                    last = self.current
                    self.current = self.path.pop()
                # if there are no more parents, we are at the root
                if len(self.path) == 0:
                    return False
            return True

        def __next__(self):
            if self.next_is_key:
                if not self.move_to_next_node():
                    raise StopIteration
                self.current_typed = self.current.reinterpret_cast(
                    self.node_p_type)
                self.next_is_key = False
                self.index += 1
                return ('key' + str(self.index), self.current_typed['key'])
            else:
                self.next_is_key = True
                return ('value' + str(self.index), self.current_typed['value'])

        def next(self):
            return self.__next__()

    def __init__(self, val):
        self.val = val
        self.size = self.val['d']['size']

    def children(self):
        d = self.val['d']

        if self.size == 0:
            return []

        real_type = self.val.type.strip_typedefs()
        key_type = real_type.template_argument(0)
        val_type = real_type.template_argument(1)
        node_type = lookup(
            'QMapData<' + key_type.name + ',' + val_type.name + '>::Node')

        return self.Iter(d['header'], node_type.pointer())

    @staticmethod
    def display_hint():
        return 'map'

    def to_string(self):
        # if we return an empty list from children, gdb doesn't print anything
        if self.val['d']['size'] == 0:
            return '<empty>'
        return "<%d items>" % self.size


class QHashNodePrinter:
    def __init__(self, val):
        self.val = val

    def children(self):
        yield ('key', self.val['key'])
        yield ('value', self.val['value'])


class QHashIteratorPrinter:

    def __init__(self, val):
        self.val = val

    def children(self):
        hash_tpl_type = self.val['i']
        q_iterator = self.val['i']['i']
        next_hash = self.val['n']['i']
        hash_tpl_type_name = hash_tpl_type.type.name[0:hash_tpl_type.type.name.rfind('::')]

        hash_tpl_type_name = lookup(hash_tpl_type_name)
        key = hash_tpl_type_name.template_argument(0)
        value = hash_tpl_type_name.template_argument(1)

        yield ("hash", self.val['c'])
        yield ("iterator", cast_ptr(q_iterator, 'QHashNode<%s, %s>' % (key, value)))
        yield ("next", cast_ptr(next_hash, 'QHashNode<%s, %s>' % (key, value)))


class QHashPrinter:
    """Print a Qt5 QHash"""

    class _Iterator:
        def __init__(self, d, e):
            self.buckets_left = d['numBuckets']
            self.node_type = e.type
            # set us up at the end of a "dummy bucket"
            self.current_bucket = d['buckets'] - 1
            self.current_node = None
            self.current_pair = None
            self.index = -1
            self.waiting_for_value = False

        def __iter__(self):
            return self

        def __next__(self):
            if self.waiting_for_value:
                self.waiting_for_value = False
                return ('value %s' % self.index, self.current_pair['value'])

            if self.current_node:
                self.current_node = self.current_node['next']

            # the dummy node that terminates a bucket is distinguishable
            # by not having its 'next' value set
            if not self.current_node or not self.current_node['next']:
                while self.buckets_left:
                    self.current_bucket += 1
                    self.buckets_left -= 1
                    self.current_node = self.current_bucket.referenced_value()
                    if self.current_node['next']:
                        break
                else:
                    raise StopIteration

            self.index += 1
            self.waiting_for_value = True
            self.current_pair = self.current_node.reinterpret_cast(self.node_type)
            return (str(self.index), self.current_pair['key'])

        def next(self):
            return self.__next__()

    def __init__(self, val):
        self.val = val
        self.size = long(self.val['d']['size'])

    def children(self):
        d = self.val['d']

        if self.size == 0:
            return []

        return self._Iterator(d, self.val['e'])

    @staticmethod
    def display_hint():
        return 'map'

    def to_string(self):
        # if we return an empty list from children, gdb doesn't print anything
        if self.size < 1:
            return '<empty>'
        return "<%d items>" % self.size


class QSetPrinter:
    """Print a Qt5 QSet"""

    def __init__(self, val):
        self.val = val
        self.size = long(self.val['q_hash']['d']['size'])
        self.is_empty = self.size == 0
        if not self.is_empty:
            setattr(self, 'children', self._children)

    def _children(self):
        hash_printer = QHashPrinter(self.val['q_hash'])
        # the keys of the hash are the elements of the set, so select
        # every other item (starting with the first)
        return itertools.islice(hash_printer.children(), 0, None, 2)

    def display_hint(self):
        if self.is_empty:
            return 'string'
        return 'array'

    def to_string(self):
        # if we return an empty list from children, gdb doesn't print anything
        if self.is_empty:
            return '<empty>'
        return "<%d items>" % self.size


class QCacheNodePrinter:
    def __init__(self, val):
        self.val = val

    def children(self):
        yield ('key', self.val['keyPtr'].referenced_value())
        yield ('value', self.val['t'].referenced_value())


class QCachePrinter:
    """Print a Qt5 QCache"""

    def __init__(self, val):
        self.val = val
        self.size = self.val['hash']['d']['size']

    def children(self):
        max_coast = self.val['mx']
        total_coast = self.val['total']

        yield ('max coast', max_coast)
        yield ('total coast', total_coast)
        # NOTE: the key appears two times
        # gdb printers limitation cannot return an iterator and tuples as children
        # at the same time
        yield ('items', self.val['hash'])

    @staticmethod
    def display_hint():
        return 'array'

    def to_string(self):
        # if we return an empty list from children, gdb doesn't print anything
        if self.size == 0:
            return '<empty>'
        return "<%d items>" % self.size


class QPointerPrinter:

    def __init__(self, val):
        self.weakptr = val['wp']
        self.tpl_type = val.type.template_argument(0)

    def children(self):
        return pointer_iter(self.weakptr, self.tpl_type)

    @staticmethod
    def display_hint():
        return 'array'


class QSharedPointerPrinter:
    """Also used by the qweakpointer"""

    def __init__(self, val):
        self.val = val

    def children(self):
        return pointer_iter(self.val)

    @staticmethod
    def display_hint():
        return 'array'


class QAtomicPointerPrinter:

    def __init__(self, val):
        self.obj = val['_q_value']['_M_b']['_M_p']

    def children(self):
        yield ('value', self.obj.referenced_value())

    @staticmethod
    def display_hint():
        return 'array'


class QPixmapPrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        _, _, _, data = split_struct("PPPP", self.val.address)
        _, width, height = split_struct("Pii", data)

        if self.val.address == 0:
            return '<invalid>'
        return "%dx%d" % (width, height)


class QVarLengthArrayPrinter:

    def __init__(self, val):
        self.val = val
        self.size = val['s']
        self.is_empty = self.size < 1

    def children(self):
        array_start = self.val['ptr']
        if self.is_empty:
            return []

        return ArrayIter(array_start, self.size)

    @staticmethod
    def display_hint():
        return 'array'

    def to_string(self):
        if self.is_empty:
            return '<empty>'
        return "<%d items>" % self.size


class QTimePrinter:

    def __init__(self, val):
        self.millis = long(val['mds'])

    @staticmethod
    def display_hint():
        return None

    def to_string(self):
        if not ms_is_valid(self.millis):
            return '<invalid>'
        return format_milliseconds(self.millis)


class QTimeZonePrinter:

    def __init__(self, val):
        self.val = val

    @staticmethod
    def display_hint():
        return 'string'

    def to_string(self):
        d = self.val['d']['d']
        if not d:
            return "NULL"
        try:
            m_id = run_method('QTimeZone', self.val.address, 'id')
        except gdb.error:
            data_offset = lookup('void').pointer().sizeof * 2  # vtable and padded qshared data
            d = cast_ptr(d, 'char', offset=data_offset)
            m_id = cast_ptr(d, 'QByteArray')
        # explicitly return the to_string value because gdb does not show
        # to_string value if there is a children method
        return QByteArrayPrinter(m_id).to_string()


class QDatePrinter:

    def __init__(self, val):
        self.val = val
        self.julian_day = long(val['jd'])
        self.is_valid = self._julian_day_is_valid(self.julian_day)
        self.can_call_locale = lookup('void').pointer().sizeof == 8
        if self.is_valid:
            setattr(self, 'children', self._children)

    def _children(self):
        if not self.is_valid:
            return
        yield ('julian day', self.val['jd'])
        yield ('to string', run_method('QDate', self.val.address, 'toString', 'Qt::TextDate'))
        yield ('ISO', run_method('QDate', self.val.address, 'toString', 'Qt::ISODate'))
        if self.can_call_locale:
            yield ('system locale', run_method('QDate', self.val.address, 'toString', 'Qt::SystemLocaleDate'))
            yield ('locale', run_method('QDate', self.val.address, 'toString', 'Qt::LocaleDate'))

    def display_hint(self):
        if not self.is_valid:
            return 'string'
        return 'array'

    def to_string(self):
        if not self.is_valid:
            return '<invalid>'
        if self.can_call_locale:
            return run_method('QDate', self.val.address, 'toString', 'Qt::TextDate')

    @staticmethod
    def _julian_day_is_valid(julian_day):
        min_julian_day = -784350574879
        max_julian_day = 784354017364
        return min_julian_day <= julian_day <= max_julian_day


class QDateTimePrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        time_t = run_method("QDateTime", self.val.address, "toSecsSinceEpoch")
        return time.ctime(long(time_t))


class QUrlPrinter:
    """Print a Qt5 QUrl"""

    def __init__(self, val):
        self.val = val

    def get_url(self):
        data = self.val['d']
        if not data:
            return '<uninitialized>'

        # TODO: add this to a table instead of generating it everytime
        int_t = lookup('int')
        qstring_t = lookup('QString')
        uchar_t = lookup('uchar')

        reader = StructReader(data)

        # from https://github.com/qt/qtbase/blob/5.15.2/src/corelib/io/qurl.cpp#L3318
        reader.add_to_ptr(int_t.sizeof)  # atomicint is the size of int
        port = long(reader.next_val(int_t))
        scheme = reader.next_val(qstring_t)
        user_name = qs_to_str(reader.next_val(qstring_t))
        reader.add_to_ptr(qstring_t.sizeof)  # size of qstring
        host = reader.next_val(qstring_t)
        path = qs_to_str(reader.next_val(qstring_t))
        query = reader.next_val(qstring_t)
        fragment = reader.next_val(qstring_t)
        reader.next_val(lookup('void').pointer())
        section_is_present = long(reader.next_val(uchar_t))
        flags = long(reader.next_val(uchar_t))

        has_scheme = section_is_present & 0x01  # QUrl::Section::Scheme
        has_query = section_is_present & 0x40  # QUrl::Section::Query
        has_fragment = section_is_present & 0x80  # QUrl::Section::Fragment
        has_authority = section_is_present & (0x02 | 0x04 | 0x08 | 0x10)  # QUrl::Section::{UserInfo | Host | Port}
        is_local_file = flags & 0x01  # QUrl::Flags::isLocalFile
        path_is_absolute = is_local_file and path.startswith('/')

        if not has_query and not has_fragment and is_local_file:
            return path

        # apparently using f"" is faster than += when concatenating strings
        url = ''
        if has_scheme:
            url = "".join((url, qs_to_str(scheme), ":"))
        if has_authority or path_is_absolute:
            url = "".join((url, "//"))
        if len(user_name) or user_name == '<uninitialized>':
            url = "".join((url, user_name, "@"))
        url = "".join((url, qs_to_str(host)))
        if port > -1:
            url = "".join((url, ":", str(port)))
        url = "".join((url, path))
        if has_query:
            url = "".join((url, "?", qs_to_str(query)))
        if has_fragment:
            url = "".join((url, "#", qs_to_str(fragment)))
        return url

    @staticmethod
    def display_hint():
        return 'string'

    def to_string(self):
        return self.get_url()


class QEasingCurvePrinter:
    """this prints an QEasingCurve enum name of the type"""

    def __init__(self, val):
        self.data = val['d_ptr']
        self.init = self.data != 0

    def to_string(self):
        if self.init:
            enum_no = long(cast_ptr(self.data, 'int').referenced_value())

            return gdb.Value(enum_no).cast(lookup("QEasingCurve::Type"))
        return "<uninitialized>"


class QFilePrinter:

    def __init__(self, val):
        _, qfile_private_ptr = split_struct('PP', long(val.address))
        filename_addr = qfile_private_ptr + self.filename_offset()
        self.filename = evaluate("*(QString *)%d" % filename_addr)

    @staticmethod
    def filename_offset():
        """
            In the private class of QFile there is a QFileDevice private before the program name
            the below is it size on different operating system and architectures

            from https://github.com/qt-creator/qt-creator/blob/4.14/share/qtcreator/debugger/qttypes.py#L657
        """
        qt_version = get_qt_version()
        qt_typeinfo = qt_type_info_version()
        is32bit = pointer_size() == 4
        if qt_version >= 0x050600 and qt_typeinfo >= 17:
            if IS_WINDOWS:
                return 160 if is32bit else 224
            else:
                return 156 if is32bit else 224
        elif qt_version >= 0x050700:
            if IS_WINDOWS:
                return 172 if is32bit else 248
            else:
                return 168 if is32bit else 248
        elif qt_version >= 0x050600:
            if IS_WINDOWS:
                return 180 if is32bit else 248
            else:
                return 168 if is32bit else 248
        elif qt_version >= 0x050500:
            return 164 if is32bit else 248
        elif qt_version >= 0x050400:
            if IS_WINDOWS:
                return 188 if is32bit else 272
            else:
                return 180 if is32bit else 272
        elif qt_version > 0x050200:
            if IS_WINDOWS:
                return 180 if is32bit else 272
            else:
                return 176 if is32bit else 272
        elif qt_version >= 0x050000:
            return 176 if is32bit else 280
        else:
            if IS_WINDOWS:
                return 144 if is32bit else 232
            else:
                return 140 if is32bit else 232

    @staticmethod
    def display_hint():
        return 'string'

    def to_string(self):
        return self.filename


class QFlagsPrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        flag_int = self.val['i']
        template_name = flag_int.type.name[0:flag_int.type.name.rfind('::')]
        template_type = lookup(template_name)
        tpl_arg = template_type.template_argument(0)

        return "%s::%s" % (tpl_arg.name, flag_int.cast(tpl_arg))


class QImagePrinter:

    def __init__(self, val):
        self.val = val
        _, _, _, self.image_data = split_struct('PPPP', self.val.address)
        self.init = False if self.image_data == 0 else True
        if self.init:
            (_, self.width, self.height, _, self.nbytes, _, _,
             _, self.q_image_format) = split_struct('iiiiqdPPi', self.image_data)

    def children(self):
        if not self.init:
            return
        yield ('width', self.width)
        yield ('height', self.height)
        yield ('nbytes', self.nbytes)
        img_format = "QImage::%s" % qimage_formats[self.q_image_format]
        yield ('format', cast_ptr(gdb.Value(img_format), 'char'))

    @staticmethod
    def display_hint():
        return 'array'

    def to_string(self):
        if self.init:
            return "%dx%d" % (self.width, self.height)
        return "<invalid>"


class QUuidPrinter:

    def __init__(self, val):
        self.val = val

    @staticmethod
    def display_hint():
        return 'string'

    def to_string(self):
        return "%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x" % (
            long(self.val['data1']), long(self.val['data2']), long(self.val['data3']),
            long(self.val['data4'][0]), long(self.val['data4'][1]),
            long(self.val['data4'][2]), long(self.val['data4'][3]),
            long(self.val['data4'][4]), long(self.val['data4'][5]),
            long(self.val['data4'][6]), long(self.val['data4'][7]))


class QDbusMessagePrinter:
    def __init__(self, val):
        self.data = val['d_ptr']

    def children(self):
        reader = StructReader(self.data)
        qstring_t = lookup('QString')
        yield ("arguments", reader.next_val(lookup('QList<QVariant>')))
        yield ("service", reader.next_val(qstring_t))
        yield ('path', reader.next_val(qstring_t))
        yield ('interface', reader.next_val(qstring_t))
        yield ('name', reader.next_val(qstring_t))
        yield ('message', reader.next_val(qstring_t))
        yield ('signature', reader.next_val(qstring_t))

    @staticmethod
    def display_hint():
        return 'array'

    @staticmethod
    def to_string():
        return "<7 items>"


class QRegExpPrinter:

    def __init__(self, val):
        self.val = val
        self.val_priv = self.val['priv']
        try:
            _, pattern_addr, self.pattern_syntax, self.case_sensitive, _, _, self.captured_cache = \
                split_struct("PPii?PP", long(self.val_priv))

            self.pattern = evaluate("(QString) %d" % pattern_addr)
            self.init = True
            # fills the capturelist cache if not there
            run_method('QRegExp', self.val.address, 'capturedTexts')
        except gdb.MemoryError:
            # since it does not exist in memory
            self.init = False

    def children(self):
        if not self.init:
            return
        yield ("pattern", self.pattern)
        yield ("patternSyntax", gdb.Value(self.pattern_syntax).cast(lookup('QRegExp::PatternSyntax')))
        yield ('caseSensitive', gdb.Value(self.case_sensitive).cast(lookup('Qt::CaseSensitivity')))

        yield ('capturedTexts', evaluate("(QStringList) %d" % self.captured_cache))

    @staticmethod
    def display_hint():
        return 'array'

    def to_string(self):
        if not self.init:
            return '<uninitialized>'
        return self.pattern


class QRegularExpressionPrinter:

    def __init__(self, val):
        self.val = val

    def to_string(self):
        data = self.val['d']['d']
        data = cast_ptr(data, 'char', offset=8)  # 2 ints QSharedData, patternOptions
        return cast_ptr(data, 'QString').referenced_value()


class QTimerPrinter:
    def __init__(self, val):
        (_, _, self.timer_id, self.interval, delete, single, nulltimer, timer_type) = \
            split_struct('PP1i3I', long(val.address))

    def children(self):
        yield ('id', self.timer_id)
        yield ('interval', "%d msecs" % self.interval)
        yield ('isActive', self.timer_id >= 0)

    @staticmethod
    def display_hint():
        return 'array'


class QSizePolicyPrinter:

    def __init__(self, val):
        self.val = val

    def children(self):
        bits = self.val['bits']
        enum_type = lookup("QSizePolicy::Policy")
        c_type = gdb.Value(1 << int(bits['ctype']))

        yield ('horizontal stretch', bits['horStretch'])
        yield ('vertical stretch', bits['verStretch'])
        yield ('horizontal policy', bits['horPolicy'].cast(enum_type))
        yield ('vertical policy', bits['verPolicy'].cast(enum_type))
        yield ('control type', c_type.cast(lookup('QSizePolicy::ControlType')))

    @staticmethod
    def display_hint():
        return 'array'


def build_qtcore_pretty_printer():
    """Builds the pretty printer for Qt5Core."""
    pp = gdb.printing.RegexpCollectionPrettyPrinter("Qt5Core")
    pp.add_printer('QAtomicInteger', '^QAtomicInteger<.*>$', QAtomicIntegerPrinter)
    pp.add_printer('QAtomicInt', '^QAtomicInt$', QAtomicIntegerPrinter)
    pp.add_printer('QBitArray', '^QBitArray$', QBitArrayPrinter)
    pp.add_printer('QByteArray', '^QByteArray$', QByteArrayPrinter)
    pp.add_printer('QCborArray', '^QCborArray$', QCborArrayPrinter)
    pp.add_printer('QCborMap', '^QCborMap$', lambda val: QCborArrayPrinter(val, is_map=True))
    pp.add_printer('QCborValue', '^QCborValue$', QCborValuePrinter)
    pp.add_printer('QChar', '^QChar$', QCharPrinter)
    pp.add_printer('QCommandLineOption', '^QCommandLineOption$', QCommandLineOptionPrinter)
    pp.add_printer('QDir', '^QDir$', QDirPrinter)
    pp.add_printer('QEasingCurve', '^QEasingCurve$', QEasingCurvePrinter)
    pp.add_printer('QEvent', '^QEvent$', QEventPrinter)
    pp.add_printer('QFile', '^QFile$', QFilePrinter)
    pp.add_printer('QFlags', '^QFlags<.*>$', QFlagsPrinter)
    pp.add_printer('QFileInfo', '^QFileInfo$', QFileInfoPrinter)
    pp.add_printer('QFileInfoList', '^QFileInfoList$', QListPrinter)
    pp.add_printer('QJsonArray', '^QJsonArray$', lambda val: QCborArrayPrinter(val, con_type='a'))
    pp.add_printer('QJsonObject', '^QJsonObject$',
                   lambda val: QCborArrayPrinter(val, con_type='o', is_map=True))
    pp.add_printer('QJsonValue', '^QJsonValue$', lambda val: QCborValuePrinter(val, is_json=True))
    pp.add_printer('QDate', '^QDate$', QDatePrinter)
    # pp.add_printer('QDateTime', '^QDateTime$', QDateTimePrinter)
    pp.add_printer('QLinkedList', '^QLinkedList<.*>$', QLinkedListPrinter)
    pp.add_printer('QList', '^QList<.*>$', QListPrinter)
    pp.add_printer('QMap', '^QMap<.*>$', QMapPrinter)
    pp.add_printer('QMultiMap', '^QMultiMap<.*>$', QMapPrinter)
    pp.add_printer('QHash', '^QHash<.*>$', QHashPrinter)
    pp.add_printer('QHashIterator', '^QHashIterator<.*>$', QHashIteratorPrinter)
    pp.add_printer('QHashNode', '^QHashNode<.*>$', QHashNodePrinter)
    pp.add_printer('QCache', '^QCache<.*>$', QCachePrinter)
    pp.add_printer('QCacheNode', '^QCache<.*>::Node$', QCacheNodePrinter)
    pp.add_printer('QPointer', '^QPointer<.*>$', QPointerPrinter)
    pp.add_printer('QWeakPointer', '^QWeakPointer<.*>$', QSharedPointerPrinter)
    pp.add_printer('QRegExp', '^QRegExp$', QRegExpPrinter)
    pp.add_printer('QRegularExpression', '^QRegularExpression$', QRegularExpressionPrinter)
    # pp.add_printer('QScopedPointer', '^QScopedPointer<.*>$', QScopedPointerPrinter)
    pp.add_printer('QSharedPointer', '^QSharedPointer<.*>', QSharedPointerPrinter)
    pp.add_printer('QQueue', '^QQueue<.*>$', QListPrinter)
    pp.add_printer('QSet', '^QSet<.*>$', QSetPrinter)
    pp.add_printer('QSizePolicy', '^QSizePolicy$', QSizePolicyPrinter)
    pp.add_printer('QStack', '^QStack<.*>$', QVectorPrinter)
    pp.add_printer('QString', '^QString$', QStringPrinter)
    pp.add_printer('QStringRef', '^QStringRef$', QStringRefPrinter)
    pp.add_printer('QStringList', '^QStringList$', QListPrinter)
    pp.add_printer('QTemporaryFile', '^QTemporaryFile$', QFilePrinter)
    pp.add_printer('QTime', '^QTime$', QTimePrinter)
    pp.add_printer('QTimeZone', '^QTimeZone$', QTimeZonePrinter)
    pp.add_printer('QVariant', '^QVariant$', QVariantPrinter)
    pp.add_printer('QVariantList', '^QVariantList$', QListPrinter)
    pp.add_printer('QVariantMap', '^QVariantMap$', QMapPrinter)
    pp.add_printer('QVector', '^QVector<.*>$', QVectorPrinter)
    pp.add_printer('QAtomicPointer', '^QAtomicPointer<.*>$', QAtomicPointerPrinter)
    pp.add_printer('QVarLengthArray', '^QVarLengthArray<.*>$', QVarLengthArrayPrinter)
    pp.add_printer('QUrl', '^QUrl$', QUrlPrinter)
    pp.add_printer('QUuid', '^QUuid$', QUuidPrinter)

    return pp


def build_qtdbus_pretty_printer():
    """Builds the pretty printer for Qt5-DBus."""
    pp = gdb.printing.RegexpCollectionPrettyPrinter("Qt5DBus")
    pp.add_printer('QDBusMessage', '^QDBusMessage$', QDbusMessagePrinter)

    return pp


def build_qtgui_pretty_printer():
    """Builds the pretty printer for Qt5-Gui."""
    pp = gdb.printing.RegexpCollectionPrettyPrinter("Qt5Gui")
    pp.add_printer('QPixmap', '^QPixmap$', QPixmapPrinter)
    pp.add_printer('QImage', '^QImage$', QImagePrinter)

    return pp


"""
    This to work on
    QElapsedTimer
        return QDateTime::currentMSecsSinceEpoch() - t1
    QIODevice #important 
    QCommandLineOption
    QXmlStreamReader 
    QXmlStreamWriter
    QLocale
    QFuture  
    QProcess
"""
"""
Class Types that do not need pretty printing
    QSize
    QSizeF
    QRect
    QRectF
    QString::Data

"""
