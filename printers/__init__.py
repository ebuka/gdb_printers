import gdb

from printers.kf5printers import build_kfcore_pretty_printer

from printers.qtprinters import build_qtcore_pretty_printer, build_qtgui_pretty_printer, build_qtdbus_pretty_printer


def register_qt_printers(obj):
    """The pretty printer for Qt5.

        registering the pretty printers for qt5
    """
    if obj is None:
        obj = gdb.current_objfile()

    gdb.printing.register_pretty_printer(obj, build_qtcore_pretty_printer())
    gdb.printing.register_pretty_printer(obj, build_qtdbus_pretty_printer())
    gdb.printing.register_pretty_printer(obj, build_qtgui_pretty_printer())

    gdb.printing.register_pretty_printer(obj, build_kfcore_pretty_printer())
