

## TODO 
- handle sigsegv when accessing method that is not yet initialised
  or only run method for variables that has been initialised

- Make sure that gdb/Mi show the correct type 
## Adding the printers to .gdbinit
to add the printer support to gdb,
*add the following to the ~/.gdbinit file*

```
python
import sys

# change the path below to the correct path 
sys.path.append('/path/to/the/printers/clone/location')

from printers import register_qt_printers
register_qt_printers(None)
end

set print pretty on
```

## AutoLoading 
This is has not been done yet 

## Running the test
to run the test 
```
run.py 
```
if it cannot find you qt headers files or 
if you have cmake prefix path in another location, run the test with 

``` 
run.py --prefix_path /the/path/to/to/qt/cmake
```


## Gdb Limitations

- if a python string is returned in the `children()` method gdb shows it as a char array instead of a string

- in GDB/MI if a variable has both `children` and `to_string` method the `to_string` is ignored and replaced with `{..}`


## Known Issues 

### Windows 
  mingw gdb version 8.1 crashes when using some printers get QDir, QFile use version gdb version 10

## Helpful links 
- [Woboq](https://code.woboq.org/)
  
  - Online Code Browser 

- [GDB python API](https://sourceware.org/gdb/onlinedocs/gdb/Python.html#Python)
  - the GDB documentation 

- [Examples of how GDB python API works](https://github.com/ruediger/Boost-Pretty-Printer/blob/master/HACKING.org)
  - Boost hacking.org